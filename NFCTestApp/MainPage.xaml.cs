﻿using NdefLibrary.Ndef;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.Proximity;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace NFCTestApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Windows.Networking.Proximity.ProximityDevice proximityDevice;


        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            initializeProximitySample();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }



        // Write a message to MessageBlock on the UI thread.
        private Windows.UI.Core.CoreDispatcher messageDispatcher = Window.Current.CoreWindow.Dispatcher;

        async private void WriteMessageText(string message, bool overwrite = false)
        {
            await messageDispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                () =>
                {
                    if (overwrite)
                        MessageBlock.Text = message;
                    else
                        MessageBlock.Text += message;
                });
        }

        private void initializeProximitySample()
        {
            proximityDevice = Windows.Networking.Proximity.ProximityDevice.GetDefault();

            if (proximityDevice == null)
                WriteMessageText("Failed to initialized proximity device.\n" +
                                 "Your device may not have proximity hardware.");
        }


        long publishedMessageId = -1;
        long subscribedMessageId = -1;

        private void PublishMessageButton_Click(object sender, RoutedEventArgs e)
        {
            // Stop publishing the current message.
            if (publishedMessageId != -1)
                proximityDevice.StopPublishingMessage(publishedMessageId);

            publishedMessageId =
                proximityDevice.PublishMessage("Windows.SampleMessage", MessageTextBox.Text);
        }

        private void SubscribeForMessageButton_Click(object sender, RoutedEventArgs e)
        {
            // Only subscribe for the message one time.
            if (subscribedMessageId == -1)
            {
                subscribedMessageId =
            proximityDevice.SubscribeForMessage("Windows.SampleMessage", messageReceived);
            }
        }

        private void messageReceived(Windows.Networking.Proximity.ProximityDevice device,
                                     Windows.Networking.Proximity.ProximityMessage message)
        {
            WriteMessageText("Message received: " + message.DataAsString + "\n");
        }

        private void StopPublishingMessageButton_Click(object sender, RoutedEventArgs e)
        {
            proximityDevice.StopPublishingMessage(publishedMessageId);
        }

        private void StopSubscribingForMessageButton_Click(object sender, RoutedEventArgs e)
        {
            proximityDevice.StopSubscribingForMessage(subscribedMessageId);
        }






        //private void btnDo_Click(object sender, RoutedEventArgs e)
        //{
        //    ProximityDevice pd = ProximityDevice.GetDefault();
        //    pd.SubscribeForMessage("NDEF", MessageReceivedHandler);
        //}



        //private void MessageReceivedHandler(ProximityDevice sender, ProximityMessage message)
        //{
        //    // Parse raw byte array to NDEF message
        //    var rawMsg = message.Data.ToArray();
        //    var ndefMessage = NdefMessage.FromByteArray(rawMsg);

        //    // Loop over all records contained in the NDEF message
        //    foreach (NdefRecord record in ndefMessage)
        //    {
        //        Debug.WriteLine("Record type: " + Encoding.UTF8.GetString(record.Type, 0, record.Type.Length));
        //        // Go through each record, check if it's a Smart Poster
        //        if (record.CheckSpecializedType(false) == typeof(NdefSpRecord))
        //        {
        //            // Convert and extract Smart Poster info
        //            var spRecord = new NdefSpRecord(record);
        //            Debug.WriteLine("URI: " + spRecord.Uri);
        //            Debug.WriteLine("Titles: " + spRecord.TitleCount());
        //            Debug.WriteLine("1. Title: " + spRecord.Titles[0].Text);
        //            Debug.WriteLine("Action set: " + spRecord.ActionInUse());
        //        }
        //    }
        //}
    }
}
